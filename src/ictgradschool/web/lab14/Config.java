package ictgradschool.web.lab14;

import java.util.Properties;

/**
 * Allows the program to get properties such as username & password from a centralized location.
 */
public class Config {

    public static final String DB_NAME = "mche618";

    private static Properties properties;

    public static Properties getProperties() {
        if (properties == null) {
            properties = new Properties(System.getProperties());
            properties.setProperty("user", "mche618");
            properties.setProperty("password", "secret21");
            properties.setProperty("useSSL", "true");
        }
        return properties;
    }

}
