package ictgradschool.web.lab14.ex3;


import ictgradschool.web.lab14.Keyboard;
import ictgradschool.web.lab14.ex2.Article;
import ictgradschool.web.lab14.ex2.ArticleDAO;
import jdk.nashorn.internal.ir.WhileNode;

import java.util.List;

/**
 * Created by mche618 on 11/05/2017.
 */
public class ex03 {
    public static void main(String[] args) {

        moviesDAO movieDatabase = new moviesDAO();
        System.out.println("Welcome to Bryan's movie collection database.");
        System.out.println("Here you will discover all kinds of craziness and randomness.");

        boolean processing = true;
        while (processing){
            System.out.println("Please select an option from the following: ");
            System.out.println("1. Information by Actor");
            System.out.println("2. Information by Movie");
            System.out.println("3. Information by Director");
            System.out.println("4. Information by Genre");
            System.out.println("5. Exit");
            System.out.println();

            try{
                int selection = Integer.parseInt(Keyboard.readInput());
                switch (selection){
                    case 1:  searchForActors(movieDatabase.getActors());
                        break;
                    case 2:  searchForMovies(movieDatabase.getMovies());
                        break;
                    case 3:  searchForDirectors(movieDatabase.getDirectors());
                        break;
                    case 4: searchForGenre(movieDatabase.getMovies());
                        break;
                    case 5:  processing =false;
                        break;
                }
            }catch (NumberFormatException e){
                System.out.println("Please enter a valid number");
            }
        }
        System.out.println("Thanks for using the database!! Bye Bye!!");
    }



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////






































    public static void searchForMovies(List<Movie> movies){

        boolean process = true;
        while (process) {
            System.out.println("Please enter the movie you would like to search or enter \"Q\" to go back: ");
            String inputTitle = Keyboard.readInput();
            if (inputTitle.equals("Q") || inputTitle.equals("q")){
                process = false;
                break;
            }
            for (Movie a: movies ){
                if(a.getName().contains(inputTitle)){
                    System.out.println("Movie name: "+a.getName());
                    System.out.println("Director: "+a.getDirector());
                    System.out.println("Year: "+a.getYear());
                    System.out.println("Genre: "+a.getGenre());
                    String cast ="Cast: ";
                    for (String actor: a.getActors()){
                        cast += actor;
                        cast += " ";
                    }
                    System.out.println();
                    process = false;
            }
            }
        }

    }



    public static void searchForDirectors(List<Director> directors){



        boolean process = true;
        while (process) {
            System.out.println("Please enter the director you would like to search or enter \"Q\" to go back: ");
            String inputTitle = Keyboard.readInput();
            if (inputTitle.equals("Q") || inputTitle.equals("q")){
                process = false;
                break;
            }
            for (Director a: directors ){
                if(a.getName().contains(inputTitle)){
                    System.out.println("Director name: "+a.getName());
                    System.out.println("Age: "+a.getAge());
                    for (String movie: a.getMovies()){
                       System.out.println(movie);
                    }
                    System.out.println();
                    process = false;
                }
            }
        }

    }



    public static void searchForActors(List<Actor> actors) {

        boolean process = true;
        while (process) {
            System.out.println("Please enter the actor you would like to search or enter \"Q\" to go back: ");
            String inputTitle = Keyboard.readInput();
            if (inputTitle.equals("Q") || inputTitle.equals("q")) {
                process = false;
                break;
            }
            for (Actor a : actors) {
                if (a.getName().contains(inputTitle)) {
                    System.out.println("Actor name: " + a.getName());
                    System.out.println("Age: " + a.getAge());
                    for (String movie : a.getActions()) {
                        System.out.println(movie);
                    }
                    System.out.println();
                    process = false;
                }
            }


        }
    }


    public static void searchForGenre(List<Movie> movies){

        boolean process = true;
        while (process) {
            System.out.println("Please enter the actor you would like to search or enter \"Q\" to go back: ");
            String inputTitle = Keyboard.readInput();
            if (inputTitle.equals("Q") || inputTitle.equals("q")) {
                process = false;
                break;
            }
            for (Movie a : movies) {
                if (a.getGenre().contains(inputTitle)) {
                    System.out.println(a.getName());
                    process = false;
                }
            }
            System.out.println();
        }
    }

}
