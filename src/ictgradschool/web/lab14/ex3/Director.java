package ictgradschool.web.lab14.ex3;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mche618 on 10/05/2017.
 */
public class Director {
    /*CREATE TABLE IF NOT EXISTS dbtest_director (
  id INT NOT NULL UNIQUE,
  name  VARCHAR(50) NOT NULL UNIQUE ,
  age INT,
  PRIMARY KEY (name)
);*/

    private int id;
    private String name;
    private int age;
    private List<String> movies;


    public Director(int id, String name, int age, List<String> movies ){
        this.id =id;
        this.name = name;
        this.age = age;
        this.movies =movies;
        if (this.movies.isEmpty()){
            this.movies = new ArrayList<String>();
            this.movies.add("All productions went out of budget and unsuccessful. The poor director is currently living on the street.");
        }
    }

    public int getId(){
        return this.id;
    }

    public String getName(){
        return this.name;
    }

    public int getAge(){
        return this.age;
    }

    public List<String> getMovies(){
        return this.movies;
    }


}
