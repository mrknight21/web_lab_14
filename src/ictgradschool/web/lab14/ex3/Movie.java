package ictgradschool.web.lab14.ex3;



import java.util.ArrayList;
import java.util.List;

/**
 * Created by mche618 on 10/05/2017.
 */
public class Movie {

    /*id INT NOT NULL UNIQUE,
  movie_name  VARCHAR(100) NOT NULL UNIQUE ,
  director  VARCHAR(50) REFERENCES dbtest_director (name),
  year INT,
  genre VARCHAR(20),*/

    private int id;
    private String name;
    private String director;
    private int year;
    private String genre;
    private List<String> actors;

    public Movie (int id, String name, String director, int year, String genre, List<String> actors ){
        this.id =id;
        this.name = name;
        this.director =director;
        this.year = year;
        this.genre = genre;
        this.actors =actors;
        if (this.actors == null){
            this.actors = new ArrayList<String>();
            this.actors.add("Bunch of inpopular rookie actors who just came out of school.");
        }
    }


    public int getId(){
        return this.id;
    }

    public String getName(){
        return this.name;
    }

    public String getDirector(){
        return this.director;
    }

    public  int getYear(){
        return this.year;
    }

    public String getGenre(){
        return this.genre;
    }

    public List<String> getActors(){
        return this.actors;
    }




}
