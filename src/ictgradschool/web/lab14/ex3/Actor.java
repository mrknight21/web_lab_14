package ictgradschool.web.lab14.ex3;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by mche618 on 10/05/2017.
 */
public class Actor {


    private int id;
    private String name;
    private int age;
    private List<String> actions;


    public Actor(int id, String name, int age, List<String> actions) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.actions = actions;
        if (this.actions.isEmpty()) {
            this.actions = new ArrayList<>();
            this.actions.add("Movie: Very real reality show called \"Life\". Role: A human trying to find a job to survive.");
        }
    }


    public int getId(){
        return this.id;
    }

    public String getName(){
        return this.name;
    }

    public int getAge(){
        return this.age;
    }

    public List<String> getActions (){
        return this.actions;
    }
}
