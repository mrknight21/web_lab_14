



DROP TABLE IF EXISTS dbtest_movie;
CREATE TABLE IF NOT EXISTS dbtest_movie (
  id INT NOT NULL UNIQUE,
  movie_name  VARCHAR(100) NOT NULL UNIQUE ,
  director  VARCHAR(50) REFERENCES dbtest_director (name),
  year INT,
  genre VARCHAR(20),
  PRIMARY KEY (movie_name)
);


INSERT INTO dbtest_movie  (id, movie_name, director, year, genre) VALUES
  (1, 'The Baby Boss', 'Dweyne Wade', 1996, 'Horror'),
  (2, 'Mahana', 'Lee Tamahori', 2004, 'Comedy'),
  (3, 'Deathgasm', 'Lee Tamahori', 2006, 'Action'),
  (4, 'Ghost Shark 2: Urban Jaws', 'Jason Lei Howden', 2004, 'Comedy'),
  (5, 'The Dead Landss', 'Dweyne Wade', 1996, 'Fiction'),
  (6, 'The Hobbit: The Battle of the Five Armies', 'Lebron James', 2002, 'Sports'),
  (7, 'Hunt for the Wilderpeople', 'Lee Tamahori', 2010, 'Thriller'),
  (8, 'Pirates of the Airways', 'Charlie Haskell', 2015, 'Horror'),
  (9, 'Field Punishment No.1', 'Jason Kidd', 2017, 'Action'),
  (10, 'What We Do in the Shadows', 'Jason Lei Howden', 2015, 'Action');




-- SELECT director FROM dbtest_movie GROUP BY director; --
DROP TABLE IF EXISTS dbtest_production;
CREATE TABLE dbtest_production AS
  SELECT movie_name, director
  FROM dbtest_movie;



DROP TABLE IF EXISTS dbtest_director;
CREATE TABLE IF NOT EXISTS dbtest_director (
  id INT NOT NULL UNIQUE,
  name  VARCHAR(50) NOT NULL UNIQUE ,
  age INT,
  PRIMARY KEY (name)
);

INSERT INTO dbtest_director (id, name, age) VALUES
  (01, 'Charlie Haskell', 55),
  (02, 'Dweyne Wade', 36),
  (03, 'Jason Kidd', 45),
  (04, 'Jason Lei Howden', 40),
  (05, 'Lebron James', 34),
  (06, 'Lee Tamahori', 27),
  (07, 'James Harden', 29);


DROP TABLE IF EXISTS dbtest_actor;
CREATE TABLE IF NOT EXISTS dbtest_actor (
  id INT NOT NULL UNIQUE,
  name  VARCHAR(50) NOT NULL UNIQUE ,
  age INT,
  PRIMARY KEY (name)
);


INSERT INTO dbtest_actor (id, name, age) VALUES
( 01,'Peter Jackson', 30 ),

( 02,'Jane Campion', 25),

( 03,'Roger Donaldson', 21),

( 04,'Temuera Morrison', 20 ),

( 05,'Russell Crowe', 19),

( 06,'Lucy Lawless', 50 ),

( 07,'Michael Hurst',27),

( 08,'Andrew Niccol', 31),

( 09,'Kiri Te Kanawa',27 ),

( 10,'Lorde', 18 ),

( 11,'Scribe', 20 ),

( 12,'Kimbra', 19);

DROP TABLE IF EXISTS dbtest_acting;
CREATE TABLE IF NOT EXISTS dbtest_acting (
  id INT NOT NULL UNIQUE ,
  movie VARCHAR(100) NOT NULL,
  actor  VARCHAR(50) NOT NULL,
  role VARCHAR(100) NOT NULL,
 PRIMARY KEY (id)
);

INSERT INTO dbtest_acting (id, movie, actor, role) VALUES
  (01, 'The Baby Boss', 'Peter Jackson', 'Dog'),
  (02, 'The Hobbit: The Battle of the Five Armies', 'Lorde', 'Cat'),
  (03, 'The Baby Boss', 'Kimbra', 'Human'),
  (04, 'Hunt for the Wilderpeople', 'Andrew Niccol', 'Tree'),
  (05, 'Hunt for the Wilderpeople', 'Peter Jackson', 'Wilder people'),
  (06, 'Pirates of the Airways', 'Michael Hurst', 'Monster'),
  (07, 'Field Punishment No.1', 'Temuera Morrison', 'Teacher'),
  (08, 'Deathgasm', 'Russell Crowe', 'Chicken'),
  (09, 'What We Do in the Shadows', 'Lucy Lawless', 'Shadow');




ALTER TABLE dbtest_movie
  ADD FOREIGN KEY (director) REFERENCES dbtest_director (name);

ALTER TABLE dbtest_production
  ADD FOREIGN KEY (movie_name) REFERENCES dbtest_movie (movie_name),
  ADD FOREIGN KEY (director) REFERENCES dbtest_director (name);

ALTER TABLE dbtest_acting
    ADD FOREIGN KEY (movie) REFERENCES dbtest_movie(movie_name),
    ADD FOREIGN KEY (actor) REFERENCES dbtest_actor (name);

