package ictgradschool.web.lab14.ex3;


import ictgradschool.web.lab14.Config;
import ictgradschool.web.lab14.ex2.Article;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by mche618 on 11/05/2017.
 */
public class moviesDAO {

    private List<Actor> actors;
    private List<Movie> movies;
    private List<Director> directors;

    moviesDAO(){

        this.actors = new ArrayList<>();
        this.movies = new ArrayList<>();
        this.directors = new ArrayList<>();
        List<String[]> actions = new ArrayList<>(); //array{movie, actor, role};//
        List<String[]> productions = new ArrayList<>();//array{movie, director};//

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/mche618", Config.getProperties())) {

            //action
            try (Statement stmt = conn.createStatement()) {
                try (ResultSet r = stmt.executeQuery("SELECT * FROM dbtest_acting;")) {
                    // Loop through each row...
                    while (r.next()) {
                        String movie = r.getString("movie");
                        String actor = r.getString("actor");
                        String role = r.getString("role");
                        String[] action =  {movie, actor, role};
                        actions.add(action);
                    }
                }
            }


            //production

            try (Statement stmt = conn.createStatement()) {
                try (ResultSet r = stmt.executeQuery("SELECT * FROM dbtest_production;")) {
                    // Loop through each row...
                    while (r.next()) {
                        String movie = r.getString("movie_name");
                        String director = r.getString("director");
                        String[] production =  {movie, director};
                        productions.add(production);
                    }
                }
            }

            //Movie

            try (Statement stmt = conn.createStatement()) {
                try (ResultSet r = stmt.executeQuery("SELECT * FROM dbtest_movie;")) {
                    // Loop through each row...
                    while (r.next()) {
                        int id = r.getInt("id");
                        String name = r.getString("movie_name");
                        String director = r.getString("director");
                        int year = r.getInt("year");
                        String genre = r.getString("genre");

                        List<String> cast = new ArrayList<>();

                        for (int i =0; i < actions.size(); i++){
                            String[] action = actions.get(i);
                            if (name.equals(action[0])){
                                String actor = action[1];
                                cast.add(actor);
                            }
                        }
                        Movie a = new Movie(id, name, director, year, genre, cast);
                        movies.add(a);
                    }
                }
            }



            //Director

            try (Statement stmt = conn.createStatement()) {
                try (ResultSet r = stmt.executeQuery("SELECT * FROM dbtest_director;")) {
                    // Loop through each row...
                    while (r.next()) {
                        int id = r.getInt("id");
                        String name = r.getString("name");
                        int age = r.getInt("age");
                        List<String> productionEx = new ArrayList<>();
                        for (int i =0; i < productions.size(); i++){
                            String[] production = productions.get(i);
                            if (name.equals(production[1])){
                                String previousProduction = "Movie: "+production[0];
                                productionEx.add(previousProduction);
                            }
                        }
                         Director a = new Director(id, name, age, productionEx);
                        directors.add(a);
                    }
                }
            }


            //Actor
            try (Statement stmt = conn.createStatement()) {
                try (ResultSet r = stmt.executeQuery("SELECT * FROM dbtest_actor;")) {
                    // Loop through each row...
                    while (r.next()) {
                       int id = r.getInt("id");
                       String name = r.getString("name");
                       int age = r.getInt("age");
                       List<String> actionEx = new ArrayList<>();
                       for (int i =0; i < actions.size(); i++){
                           String[] action = actions.get(i);
                           if (name.equals(action[1])){
                               String previousAct = "Movie: "+action[0]+". Role: "+action[2]+".";
                               actionEx.add(previousAct);
                           }
                    }
                    Actor a = new Actor(id, name, age, actionEx);
                        actors.add(a);
                }
            }
        }
    }catch (SQLException e) {
            e.printStackTrace();
        }
}


    public List<Actor> getActors(){
        return this.actors;
    }


    public List<Movie> getMovies(){
        return this.movies;
    }

    public List<Director> getDirectors(){
        return this.directors;
    }

}
