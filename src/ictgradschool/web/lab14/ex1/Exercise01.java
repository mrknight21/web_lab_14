package ictgradschool.web.lab14.ex1;

import ictgradschool.web.lab14.Config;
import ictgradschool.web.lab14.Keyboard;

import java.sql.*;
import java.util.Properties;

public class Exercise01 {







    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */

        int indicator = 0;
        while (indicator == 0) {
            System.out.println("Please enter the title you would like to search: ");
            String inputTitle = Keyboard.readInput();


            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Set the database name to your database
            try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/mche618", Config.getProperties())) {
                try (Statement stmt = conn.createStatement()) {
                    try (ResultSet r = stmt.executeQuery("SELECT title, body FROM simpledao_articles WHERE simpledao_articles.title LIKE \"%"+ inputTitle+"%\"")) {

                        // Loop through each row...
                        while (r.next()) {

                           System.out.println("Article Author: "+r.getString("title"));
                           System.out.println("Article content: "+r.getString("body"));
                           indicator++;
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
