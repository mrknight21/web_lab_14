package ictgradschool.web.lab14.ex2;

import ictgradschool.web.lab14.Config;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mche618 on 10/05/2017.
 */
public class ArticleDAO {


    public List<Article> allArticles;


    public ArticleDAO() {
        // db connection and query
        List<Article> articles = new ArrayList<>();


        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/mche618", Config.getProperties())) {
            try (Statement stmt = conn.createStatement()) {
                try (ResultSet r = stmt.executeQuery("SELECT title, body FROM simpledao_articles;")) {
                    // Loop through each row...
                    while (r.next()) {
                        Article a = new Article();
                        a.setTitle(r.getString("title"));
                        a.setContent(r.getString("body"));
                        articles.add(a);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        this.allArticles = articles;
    }


    public List<Article> getAllArticles() {
        return this.allArticles;
    }

    public List<Article> PrintArticle(String inputTitle) {
        List<Article> articles = new ArrayList<>();
        for (Article a : allArticles) {
            if (a.getTitle().contains(inputTitle)) {
                articles.add(a);
            }
        }
        return articles;
    }

    ;


}
