package ictgradschool.web.lab14.ex2;

/**
 * Created by mche618 on 10/05/2017.
 */
public class Article {

    private String title;
    private String content;

    public void setTitle(String s) {
        this.title= s;
    }

    public void setContent(String s){
        this.content = s;
    };

    public String getContent(){
        return this.content;
    };


    public String getTitle(){
        return this.title;
    };



}
