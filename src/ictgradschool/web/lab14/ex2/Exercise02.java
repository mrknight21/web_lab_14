package ictgradschool.web.lab14.ex2;

import ictgradschool.web.lab14.Config;
import ictgradschool.web.lab14.Keyboard;

import java.sql.*;
import java.util.Iterator;
import java.util.List;

public class Exercise02 {







    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        ArticleDAO search = new ArticleDAO();

        while (true) {
            System.out.println("Please enter the title you would like to search: ");
            String inputTitle = Keyboard.readInput();
            List<Article> result = search.PrintArticle(inputTitle);
            if (!result.isEmpty()){
                for(Article b : result){
                    System.out.println("Article Author: "+b.getTitle());
                    System.out.println("Article content: "+b.getContent());
                }
                break;
            }
            else {
                System.out.println("Please try search again!");
            }
        }
    }
}


